## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP
source ./phases/sendCurl.sh sendCurl
source ./phases/uploadS3.sh uploadS3

## Setting variables
name=${1}
echo "Process name : ${name}"

INPUT=$(jq ".directoryRoot" --raw-output "./config.json")
#dir_input="${INPUT}/${rootFolder}"
dir_input="${INPUT}"
echo "Data directory : ${dir_input}"

## Taking config variables
source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"
uploadProcess="$(jq ".directoryActions[] | select(.name == \"${name}\").uploadProcess" --raw-output "./config.json")"
serverName="$(jq ".directoryActions[] | select(.name == \"${name}\").serverName" --raw-output "./config.json")"
serverFolder="$(jq ".directoryActions[] | select(.name == \"${name}\").serverFolder" --raw-output "./config.json")"
checkDelay="$(jq ".checkDelay" --raw-output "./config.json")"

id=0

echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Debut verrouillage fichiers"
espace

for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  for foundFile in $FILES; do
    ## checkCommand checks if the file is old enough
    checkCommand="find ${foundFile} -mmin +${checkDelay} -type f"
    #echo "${checkCommand}"
    if [ "${foundFile}" != "${FILES}" ]; then
      for file in $(eval "${checkCommand}"); do
        sendCurl "${name}" "${file}" "Fichier détecté"
        echo "Le fichier detecté est   : ${file}"
        fileExtension=$(getExt "${file}" "1")
        echo "L'extension detecté est  : ${fileExtension}"
        file=$(addExt "${file}" "lock")
        echo "Le fichier est devenu    : ${file}"
        ((id += 1))
        echo "Number of file processed : $id"
        sendCurl "${name}" "${file}" "Fichier verouillé"
      done
    else
      echo "No compatible file detected for ${acceptedFile}"
    fi
    espace
  done
done

echo "Fin verrouillage fichiers"
separateur

#######################################################################################################################

echo "Début upload"
espace

FOLDER="${source}/*"
uploading="${source}/uploading"
uploaded="${source}/uploaded"
errorFolder="${source}/error/"

for file in $FOLDER; do
  #Variables
  filename=$(getFilename "${file}" "1")
  status=0

  ## Check if file is being written on
  if [[ ${file} == *.lock ]] && [ ${id} != 0 ]; then
    ## Starting message
    sendCurl "${name}" "${file}" "Début upload"
    echo "Starting ${filename} upload"

    ## Removing lock extension
    file=$(cutExt "${file}")
    mv "${file}" "${uploading}/${filename}"
    file="${uploading}/${filename}"
    echo "The file is now ${file}"
    espace

    ## Removing others extensions
    #removeExt=0
    #removeExt="$(jq ".directoryActions[] | select(.name == \"${name}\").removeExtensions" --raw-output "./config.json")"
    #realExt=$(getExt "${file}" "1")
    #echo "The real extension is : ${realExt}"
    #removeExt=$(($removeExt+1))
    #echo "Extensions to remove  : ${removeExt}"
    #filename=$(getFilename "${file}" "${removeExt}")
    #echo "The filename is       : ${filename}"
    #echo "The file will be      : ${filename}.${realExt}"
    #mv -v "${file}" "${uploading}/${filename}.${realExt}"
    #file="${uploading}/${filename}.${realExt}"
    #espace

    #Upload functions
    echo "Server name : ${serverName}"
    case ${uploadProcess} in
      scp)
        echo "Doing SCP"
        output=$(secureCopy "${serverName}" "${name}" "${file}" 2>&1)
        ;;
      sftp)
        echo "Doing SFTP"
        output=$(safeFTP "${serverName}" "${name}" "${file}" 2>&1)
        ;;
      s3)
        echo "Doing S3"
        bucket="$(jq ".serverLogin[] | select(.name == \"${serverName}\").bucket" --raw-output "./config.json")"
        #echo "Bucket : ${bucket}"
        profile="$(jq ".serverLogin[] | select(.name == \"${serverName}\").profile" --raw-output "./config.json")"
        #echo "Profile : ${profile}"
        output=$(uploadS3 "${file}" "${bucket}" "${serverFolder}" "${filename}" "${profile}" 2>&1)
        #uploadS3 "${file}" "${bucket}" "${serverFolder}" "${filename}" "${profile}"
        ;;
      test)
        echo "Simulating upload"
        echo "Bip boup le fichier upload bzzzz"
    esac

    ## Error display
    status=$?
    output=$(echo "${output}" | tail -n1)
    echo "Output        : ${output}"
    echo "Upload status : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      ## Send Error message and status
      sendCurl "${name}" "${file}" "Fichier non uploadé"
      sendCurl "${name}" "${file}" "${output}"
      echo "ERROR WITH THE UPLOAD (code : ${status})"
      echo "The directory ${file} has NOT been correctly uploaded !!!"
      mv -v "${file}" "${errorFolder}"
    else
      sendCurl "${name}" "${file}" "Fichier uploadé"
      ## Move the file to not upload it again
      mv "${file}" "${uploaded}"
      echo "The file ${file} has been correctly uploaded"
    fi
  else
    echo "Le fichier ${filename} n'est pas à upload"
  fi
  espace
done

echo "Upload terminé"
separateur
separateur
separateur
separateur
separateur