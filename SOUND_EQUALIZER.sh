## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

source ./phases/encode.sh encoding
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP
source ./phases/sendCurl.sh sendCurl
source ./phases/importColors.sh importColors
source ./phases/soundParser.sh soundParser
source ./phases/soundEqual.sh soundEqual

importColors
## Setting variables
name=SOUND_EQUALIZER

dir_input="$(jq ".directoryRoot" --raw-output "./config.json")"

source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
encodingSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoding"
encodedSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoded"
soundCfgSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/soundCfg"

destSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").destinationFolder" --raw-output "./config.json")"

acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"

id=0

echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Debut verrouillage fichiers"
espace

for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  for file in $FILES; do
    if [ "${file}" != "${FILES}" ]; then
      sendCurl "${name}" "${file}" "Fichier détecté"
      echo "Le fichier detecté est   : ${file}"
      fileExtension=$(getExt "${file}" "1")
      echo "L'extension detecté est  : ${fileExtension}"
      filename=$(getFilename "${file}" "1")
      echo "Le nom du fichier est    : ${filename}"
      file=$(addExt "${file}" "lock")
      echo "Le fichier est devenu    : ${file}"
      ((id += 1))
      echo "Number of file processed : $id"
      sendCurl "${name}" "${file}" "Fichier verouillé"
    else
      echo "No compatible file detected for ${acceptedFile}"
    fi
    espace
  done
done

echo "Fin verrouillage fichiers"
separateur

#######################################################################################################################

## Taking parameters from config file
I="$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.I" --raw-output "./config.json")"
LRA="$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.LRA" --raw-output "./config.json")"
tp="$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.tp" --raw-output "./config.json")"
audioChannel="$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioChannel" --raw-output "./config.json")"
audioSamplingFrequency="$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioSamplingFrequency" --raw-output "./config.json")"


echo "I = ${I}"
echo "LRA = ${LRA}"
echo "tp = ${tp}"
echo "audioChannel = ${audioChannel}"
echo "audioSamplingFrequency = ${audioSamplingFrequency}"

echo "Début equalizer"
espace

DIR="${source}/*"
for file in $DIR; do
  if [[ ${file} == *.lock ]] && [ ${id} != 0 ]; then
    ## Starting message
    status=0
    sendCurl "${name}" "${file}" "Début de l'encodage du fichier"

    ## File lock and identification
    whatFile
    filename=$(getFilename "${file}" "2")
    echo "Filename       : ${filename}"
    fileExtension=$(getExt "${file}" "2")
    echo "File extension : ${fileExtension}"
    mv "${file}" "${encodingSource}/${filename}.${fileExtension}"
    file="${encodingSource}/${filename}.${fileExtension}"
    soundConfigFile="${soundCfgSource}/${filename}.json"
    #touch "${soundConfigFile}"

    ## Debug functions TODO move functions
    #output=$(soundParser "${I}" "${LRA}" "${tp}" "${file}" 2>&1 | tail -n12)
    #echo "${output}" > "${soundConfigFile}"

    ## Settings variables
    measured_I="$(jq ".input_i" --raw-output "${soundConfigFile}")"
    measured_LRA="$(jq ".input_lra" --raw-output "${soundConfigFile}")"
    measured_tp="$(jq ".input_tp" --raw-output "${soundConfigFile}")"
    measured_thresh="$(jq ".input_thresh" --raw-output "${soundConfigFile}")"
    target_offset="$(jq ".target_offset" --raw-output "${soundConfigFile}")"

    echo "Mesures : "
    echo "${measured_I}"
    echo "${measured_LRA}"
    echo "${measured_tp}"
    echo "${measured_thresh}"
    echo "${target_offset}"
    echo "${destSource}/${filename}.${fileExtension}"

    soundEqual "${I}" "${LRA}" "${tp}" "${file}" "${measured_I}" "${measured_LRA}" "${measured_tp}" "${measured_thresh}" "${target_offset}" "${destSource}/${filename}.loudnorm.${fileExtension}" "${audioChannel}" "${audioSamplingFrequency}"

    ## Encoding function
    #output=$(addFade "${fileExtension}" "${fileExtension}" "${encodingSource}" "${destSource}" "${fadeIn}" "${fadeOut}" "${blackScreen}" "${encodingSrcConfig}" 2>&1) # both stdout and stderr will be captured
    status=$?

    ## Error display
    output=$(echo "${output}" | tail -n1)
    echo "Output               : ${output}"
    echo "SOUND_EQUALIZER encoding status : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      echo "${RED}ERROR WITH SOUND_EQUALIZER (code : ${YELLOW}${status}${RED})${NOCOLOR}"
      ## Send Error message and status
      sendCurl "${name}" "${file}" "Fichier non encodé"
      sendCurl "${name}" "${file}" "${output}"
      echo "Le fichier ${filename} n'a pas été encodé"
    else
      sendCurl "${name}" "${file}" "Fichier encodé"
      echo "Le fichier ${filename} a été encodé"
    fi

    ## Moving file after encoding
    mv "${file}" "${encodedSource}/${filename}.${fileExtension}"

  else
    filename=$(getFilename "${file}" "0")
    echo "Le fichier ${filename} n'est pas à encoder"
  fi

  espace
done

echo "Equalizer terminé"
separateur
separateur
separateur
separateur
separateur
