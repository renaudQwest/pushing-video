<?php

/******************************************/
/*ON RECUP LISTE DES FICHIERS D'UN DOSSIER*/
/******************************************/

function getDirContents($dir,$joker=null){
    
    $results=null;
    try
    {
	    if(is_dir($dir))
    	{
    		$files = scandir($dir);
    		
    		$files = array_diff(scandir($dir), array('..', '.'));
    		
    		if($files !==false)
    		foreach($files as $key => $value)
    		{
    		    $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
    
        		if(!is_dir($path)) 
        		{
            		if($joker)
            		{
            			if(strrpos($path,'.')==strrpos($path,'.'.$joker))
							$results[] = $path;				
            		}
            		else
            			$results[] = $path;
        		} 
     		}
    	}
    }catch(Exception $e)
	{
	}    
	return $results;
}

/************************************************/
/*		MAIN									*/
/************************************************/
					
//On recup fichier de config
$myJsonString = file_get_contents("./config.json");
$myJsonConfig = json_decode($myJsonString, true);

if(isset($myJsonConfig['directoryRoot']))
	$myRootDir=$myJsonConfig['directoryRoot'];


//on parcourt chacun des dir Actions
foreach ($myJsonConfig['directoryActions'] as $dirAction)
{
  echo $dirAction['name']."\n";
  $mySourceDir=$dirAction['sourceFolder'];
  $actionExec=$dirAction['action'];

  //on regarde si on trouve des fichiers avec le bon joker
  foreach ($dirAction['acceptedFiles'] as $acceptedFiles)
  {
    foreach (explode(" ",$acceptedFiles) as $acceptedFile)
    {
      //echo "$myRootDir/$subDir/$mySourceDir/*.$acceptedFile\n";
      $myFileNames=getDirContents("$myRootDir/$mySourceDir",$acceptedFile);

      if($myFileNames)
      foreach($myFileNames as $myFileName)
      {
          echo "A candidate : $myFileName \n";
          echo "$actionExec $mySubDir\n";
          shell_exec("$actionExec $subDir");
          break;
      }
    }
  }
  echo "\n<-------------------->\n\n";
}

	