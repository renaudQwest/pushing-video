#!/bin/bash
source ./.env.sh my_aws_access_key my_aws_access_secret my_aws_account_id my_aws_region my_aws_bucket

export aws_access_key="${my_aws_access_key}"
export aws_access_secret="${my_aws_access_secret}"
export aws_account_id="${my_aws_account_id}" # All number must be attached (must don't to be XXXX-XXXX-XXXX)
export aws_region_id="${my_aws_region}"
export aws_archive_vault_name="${my_aws_archive_vault_name}"
export aws_bucket="${my_aws_bucket}"