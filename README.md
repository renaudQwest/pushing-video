# Tested
* Tested on Mac OS X Catalina (10.15.7)
* Partially tested on Debian 9

# Requirement
```
 • ffmpeg
 • jq
 • aws-cli
 • treehash
```

## Update
```shell script
sudo apt-get update
```

## jq
[jq](https://stedolan.github.io/jq/) to serialize json and config manipulation
```shell script
sudo apt-get install jq
```

## awscli
```shell script
sudo apt-get install awscli
```

## ffmpeg
### Ubuntu 3.X.X
```shell script
sudo apt-get install ffmpeg
```
### Ubuntu 4.X.X
```shell script
sudo snap install ffmpeg
```
### Debian
```shell script
sudo apt-get install ffmpeg
```
### CentOS
```shell script
sudo yum install ffmpeg
```
### Mac OS X
#### with homebrew
```shell script
brew install ffmpeg
``` 

# Starting
## Before starting
Create a file `config.json` like :
```json
{
  "directoryRoot" : "Name of root folder", 
  "directoryActions":[
      {
        "name" : "Name of cycle",
        "sourceFolder" : "Input folder",
        "destinationFolder" : "Output Folder",
        "acceptedFiles" : [ "name of extension video file" ],
        "variables": {
          "transcode": {
            "videoCodec"   : "ffmpeg video codec",
            "videoBitrate" : "ffmpeg video bitrate in MBits per second",
            "videoFilter"  : "ffmpeg video filters",
            "audioCodec"   : "ffmpeg audio codec",
            "audioBitrate" : "ffmpeg audio bitrate in kBits per second",
            "audioChannel" : "ffmpeg audio channel in unit",
            "audioSamplingFrequency" : "ffmpeg audio sampling frequency in kHz"
        }
      },
      "action" : "script and options to launch"
    }
  ],
  "s3Credentials" : [
    {
      "name"      : "Name of provider",
      "type"      : "Video type (SVOD, AVOD, FAST)",
      "accessKey" : "AWS access key id",
      "secretKey" : "AWS secret key id",
      "region"    : "AWS region id (eu-west-1…)",
      "vault"     : "AWS vault of provider",
      "bucket"    : "AWS destination bucket"
    }
  ]
}
``` 

Create a directory with the same name as the directoryActions object and change the value in the config file.
It's the property `sourcefolder` in `config.json` from `directoryActions`.
```shell script
source ./phases/encode.sh encoding
    
name=HD_16_9
    
file=$(basename -- "$1")

videoCodec=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.videoCodec" --raw-output "./config.json")
videoBitrate=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.videoBitrate" --raw-output "./config.json")
videoFilter=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.videofilter" --raw-output "./config.json")
audioCodec=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.audioCodec" --raw-output "./config.json")
audioBitrate=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.audioBitrate" --raw-output "./config.json")
audioChannel=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.audioChannel" --raw-output "./config.json")
audioSamplingFrequency=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").variables.transcode.audioSamplingFrequency" --raw-output "./config.json")
    
source=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").sourceFolder" --raw-output "./config.json")
destSource=$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").destinationFolder" --raw-output "./config.json")
    
encoding "${HOME}/${source}/${file}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${videoFilter}" "${audioChannel}" "${audioSamplingFrequency}" "${HOME}/${destSource}/${file}"
```
and launch command
```shell script
bash setup.sh
```
The script will create the root folder and sub-folders.

To create a watch folder with

```shell script
crontab -e
```
when vi is open, add :
```
*/15 * * * *  path/to/directory/at/watch path/to/script/at/run > file.log
```

# Process list
By default, all files output in the `_QC` folder.

## LONG_FORMS
Input folders : 
* `ENCODE_SL_HD_16_9` for standard 1080p video
* `ENCODE_SL_HD_16_9_NTSC` for 1080p NTSC video
* `ENCODE_SL_HD_16_9_NTSC_PROGRESSIF` for 1080p progressive NTSC video
* `ENCODE_SL_UHD_4K` for 4k files

Encode input files with the configuration provided by `config.json`.

## SHORTS
Input folder : `ENCODE_SHORTS`

Output folder : `ENCODE_FADE`

Cut the video file with the timestamps provided by timestamp file.

Needs :
* Timestamp file with the same name as the video file but with `.txt` extension.

(Example : for `video.mp4`, the timestamp will be `video.txt`)
* The timestamp must follow the following format :
```shell script
00:00:00.00,00:00:10.00
00:00:12.00,00:00:35.18
00:01:00.00,00:02:37.04
```
This will produce 3 different videos in the output folder.

## FADE
Input folder : `ENCODE_FADE`

Adds a video and audio fading at the start and the end of the video file.
You can configure the fade duration in `config.json`.

## (WIP) FUSION_MONO
Input folder : `ENCODE_FUSION_MONO`

Encode the video file and create 1 stereo audio stream with 2 mono audio streams.
Mostly for `.mxf` source files.