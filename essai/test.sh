#!/bin/sh

totalSize=$(wc -c <"${HOME}/Desktop/test.mp4" | xargs);
size=1048575

getPercentage() {
  echo $(($1 * 100 / $2)) '% sended |' $1 / $2
}

for item in $(seq $size $size $totalSize); do
  getPercentage ${$((item))%.*} $totalSize
done
