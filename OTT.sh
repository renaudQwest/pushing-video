## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

source ./phases/broadcastOTT.sh broadcastOTT
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur

name="OTT"

dir_input="${HOME}/INPUT/${rootFolder}"

profileName=$(jq ".directoryActions[] | select(.name == \"${name}\").name" --raw-output "./config.json")
source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
destSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").destinationFolder" --raw-output "./config.json")"
bucket=$(jq ".s3Credentials[] | select(.type == \"${name}\").bucket" --raw-output "./config.json")

acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"

case ${rootFolder} in
JAZZ)
  folder="QWJAZ"
  ;;
MIX)
  folder="QWMIX"
  ;;
CLASSIC)
  folder="QWCLA"
  ;;
esac

##TODO : remove DEV folder
folder="QWDEV"

echo "${folder}"

id=0

echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Début verrouillage fichiers pout OTT"
espace

for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  for file in $FILES; do
    if [ "${file}" != "${FILES}" ]; then
      echo "Le fichier detecté est   : ${file}"
      fileExtension=$(getExt "${file}" "1")
      echo "L'extension detecté est  : ${fileExtension}"
      file=$(replaceExt "${file}" "readyToUpload")
      echo "Le fichier est devenu    : ${file}"
      ((id += 1))
      echo "Number of file processed : $id"
    else
      echo "No compatible file detected for ${acceptedFile}"
    fi
    espace
  done
done

echo "Fin verrouillage fichiers pour OTT"
separateur

echo "Début upload"
espace

DIR="${source}/*"
for file in $DIR; do
  if [[ ${file} == *.readyToUpload ]] && [ ${id} != 0 ]; then
    #Pour éviter d'encoder 2 fois le même fichier
    file=$(replaceExt "${file}" "uploading")
    whatFile

    filename=$(getFilename "${file}" "2")
    echo "Filename : ${filename}"

    broadcastOTT "${file}" "${bucket}" "${folder}" "${filename}.mp4" "${profileName}"
    #cp "${file}" "${destSource}/${filename}.mp4"
    file=$(replaceExt "${file}" "uploaded")
    mv "${file}" "${destSource}/${filename}.finished"

    echo "Le fichier ${filename} a été upload"
    espace
  else
    echo "Pas de fichiers à upload"
  fi
done

espace
echo "Upload terminé"
espace
