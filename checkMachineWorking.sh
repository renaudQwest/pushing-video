MW_dir_input="$(jq ".directoryRoot" --raw-output "./config.json")"
echo "Input folder : ${MW_dir_input}"

MW_tmp_folder="${MW_dir_input}/.tmp"
mkdir -p "${MW_tmp_folder}"
echo "Temp folder  : ${MW_tmp_folder}"

echo "Machine name : $HOSTNAME"

MW_tmp_file="${MW_tmp_folder}/${HOSTNAME}.txt"
echo "Temp file    : ${MW_tmp_file}"

startMachineWork() {
  touch "${MW_tmp_file}"
  echo "$MW_process_name" > "$MW_tmp_file"
}

isMachineWorking() {
  if [[ -f "${MW_tmp_file}" ]]; then
    #echo "Occupe"
    return 0
  else
    #echo "Libre"
    return 1
  fi
}

stopMachineWork() {
  rm "${MW_tmp_file}"
}

checkMachineWorking() {
  MW_process_name=$1
  echo "Running machine check for : $MW_process_name"
  if ! isMachineWorking; then
    startMachineWork
    echo "Machine available"
  else
    echo -e "${RED}Machine already working${NOCOLOR}"
    exit 1
  fi
}


if ((0)); then
  echo "Check work 1"
  isMachineWorking
  sleep 1

  echo "Start work 1"
  startMachineWork
  sleep 1

  echo "Check work 2"
  if ! isMachineWorking; then
    startMachineWork
    echo "Start work 2"
  else
    echo "Machine already running"
  fi
  sleep 1

  echo "Stop work 1"
  stopMachineWork
  sleep 1

  echo "Check work 3"
  if ! isMachineWorking; then
    startMachineWork
    echo "Start work 3"
  fi
  sleep 1

  echo "Stop work 3"
  stopMachineWork
  sleep 1
fi
