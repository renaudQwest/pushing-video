directoryRoot="$(jq ".directoryRoot" --raw-output "./config.json")"

mkdir -v "${directoryRoot}"

length=$(jq ".directoryActions | length" "./config.json")
echo "Action number : ${length}"
echo "   "

for (( i=0; i<"${length}"; i++ )) ; do
  echo "Action : ${i}"
  action=$(jq ".directoryActions[${i}].name" --raw-output "./config.json")
  echo "${action}"
  sourceFolder=$(jq ".directoryActions[${i}].sourceFolder" --raw-output "./config.json")
  echo "${sourceFolder}"

  mkdir -v "${directoryRoot}/${sourceFolder}"

  subFoldersLength=$(jq ".directoryActions[${i}].subFolders | length" "./config.json")
  echo "Sub number               : ${subFoldersLength}"
  echo "   "

  for (( f=0; f<"${subFoldersLength}"; f++ )) ; do
    echo "Sub Folder : ${f}"
    subFolder=$(jq ".directoryActions[${i}].subFolders[${f}]" --raw-output "./config.json")
    echo "Sub folder ${f} : ${subFolder}"

    mkdir -v "${directoryRoot}/${sourceFolder}/${subFolder}"

    echo "   "
  done

  commonSubFoldersLength=$(jq ".commonSubFolders | length" "./config.json")
  echo "Common SubFolders number : ${commonSubFoldersLength}"
  echo "   "

  for (( f=0; f<"${commonSubFoldersLength}"; f++ )) ; do
    echo "Common Sub Folder : ${f}"
    commonSubFolder=$(jq ".commonSubFolders[${f}]" --raw-output "./config.json")
    echo "Common Sub folder ${f} : ${commonSubFolder}"

    mkdir -v "${directoryRoot}/${sourceFolder}/${commonSubFolder}"

    echo "   "
  done

  echo "   "
done

length=$(jq ".outputFolders | length" "./config.json")
echo "Output number : ${length}"
echo "   "

for (( i=0; i<"${length}"; i++ )) ; do
  echo "Output : ${i}"
  sourceFolder=$(jq ".outputFolders[${i}].sourceFolder" --raw-output "./config.json")
  echo "${sourceFolder}"

  mkdir -v "${directoryRoot}/${sourceFolder}"

  subFoldersLength=$(jq ".outputFolders[${i}].subFolders | length" "./config.json")
  echo "Sub number : ${subFoldersLength}"
  echo "   "

  for (( f=0; f<"${subFoldersLength}"; f++ )) ; do
    echo "Sub Folder : ${f}"
    subFolder=$(jq ".outputFolders[${i}].subFolders[${f}]" --raw-output "./config.json")
    echo "Sub folder ${f} : ${subFolder}"

    mkdir -v "${directoryRoot}/${sourceFolder}/${subFolder}"

    echo "   "
  done

  echo "   "
done