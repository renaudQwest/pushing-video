cutting() {
  ## 1= source file, 2= time in, 3= time out, 4= destination file
  ffmpeg -i "${1}" -ss "${2}" -to "${3}" -y "${4}"
}