#!/bin/bash

source ./phases/importColors.sh importColors
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/cutting.sh cutting

encoding() {
  echo "File status               : ${10}"
  echo "(0 : default)"
  echo "(1 : pad)"
  echo "(2 : stereo)"
  echo "Input File                : $1, "
  echo "Codec Video               : $2, "
  echo "Video Bitrate             : $3, "
  echo "Audio Codec               : $4, "
  echo "Audio Bitrate             : $5, "
  echo "Video Filter              : $6, "
  echo "Audio Channel             : $7, "
  echo "Audio Sampling Frequency  : $8, "
  echo "Output File               : $9, "
  echo "Timestamp File            : ${11}"
  echo "Height                    : ${12}"
  echo "Width                     : ${13}"
  importColors

  num=0
  if [[ -f "${11}" ]]; then
    echo "Timestamp exists"
    while IFS= read -r line || [[ -n "$line" ]]; do
      if [[ $line =~ [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9],[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9]$ ]]; then
        num=$((num+1))
        espace
        echo "Line number        : $num"
        echo "Text read from file: $line"
        timeIn=$(echo "${line}" | tr , \\n | head -n 1)
        timeOut=$(echo "${line}" | tr , \\n | tail -n 1)
        echo "Time IN  : ${timeIn}"
        echo "Time OUT : ${timeOut}"

        declare "timeIn_${num}=$(echo "${line}" | tr , \\n | head -n 1)"
        declare "timeOut_${num}=$(echo "${line}" | tr , \\n | tail -n 1)"
      fi
    done < "${11}"

    separateur
    echo "Number of shorts : $num"

    if [[ ${num} == 0 ]];then
      exit 1
    fi

    for (( i=1; i<="${num}"; i++ )) ; do
      echo "Num : ${i}"

      var1="timeIn_${i}"
      var2="timeOut_${i}"
      echo "Dime IN  : ${!var1}"
      echo "Dime OUT : ${!var2}"

      if [ "${10}" == "0" ]; then
        ffmpeg -nostdin -hide_banner -loglevel error -i "$1" -c:v "$2" -ss "${!var1}" -to "${!var2}" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "scale=w=${13}:h=${12}, setdar=16/9, ${6}" -aspect 16:9 -timecode "00:00:00.00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
      elif [ "${10}" == "1" ]; then # ih*16/9:ih:(ow-iw)/2:0
        ffmpeg -nostdin -hide_banner -loglevel error -i "$1" -c:v "$2" -ss "${!var1}" -to "${!var2}" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "scale=(iw*sar)*min(${13}/(iw*sar)\,${12}/ih):ih*min(${13}/(iw*sar)\,${12}/ih), pad=${13}:${12}:(${13}-iw*min(${13}/iw\,${12}/ih))/2:(${12}-ih*min(${13}/iw\,${12}/ih))/2, ${6}" -aspect 16:9 -timecode "00:00:00.00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
      elif [ "${10}" == "2" ]; then # stereo audio
        ffmpeg </dev/null -nostdin -hide_banner -loglevel error -i "$1" -c:v "$2" -ss "${!var1}" -to "${!var2}" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter_complex "[a:0][a:1]amerge=inputs=2[ch1ch2];scale=w=${13}:h=${12}, setdar=16/9, ${6}" -map '[ch1ch2]' -aspect 16:9 -timecode "00:00:00.00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
      fi
    done
  else
    if [ "${10}" == "0" ]; then
      ffmpeg -nostdin -hide_banner -loglevel error -i "$1" -c:v "$2" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "scale=w=${13}:h=${12}, setdar=16/9, ${6}" -aspect 16:9 -timecode "00:00:00.00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
    elif [ "${10}" == "1" ]; then # ih*16/9:ih:(ow-iw)/2:0
      ffmpeg -nostdin -hide_banner -loglevel error -i "$1" -c:v "$2" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "scale=(iw*sar)*min(${13}/(iw*sar)\,${12}/ih):ih*min(${13}/(iw*sar)\,${12}/ih), pad=${13}:${12}:(${13}-iw*min(${13}/iw\,${12}/ih))/2:(${12}-ih*min(${13}/iw\,${12}/ih))/2, ${6}" -aspect 16:9 -timecode "00:00:00.00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
    elif [ "${10}" == "2" ]; then # ih*16/9:ih:(ow-iw)/2:0
      ffmpeg </dev/null -nostdin -hide_banner -loglevel error -i "$1" -c:v "$2" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter_complex "[a:0][a:1]amerge=inputs=2[ch1ch2];scale=w=${13}:h=${12}, setdar=16/9, ${6}" -map '[ch1ch2]' -aspect 16:9 -timecode "00:00:00.00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
    fi
  fi
}