## Imports
source ./phases/sendCurl.sh sendCurl
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur getLineNumberOfFile

fileLock() {
  echo "File lock"

  ## Variables
  echo "Input directory           : $1, "
  echo "Output directory          : $2, "
  echo "New extension             : $3, "
  #echo "Accepted files            : $4, "
  echo "Process name              : $5,"
  echo "Process log file          : $6,"
  echo "Number of files to take   : $7"
  inputDirectoryLOCK="${1}"
  outputDirectoryLOCK="${2}"
  newExtensionLOCK="${3}"
  acceptedFilesLOCK="${4}"
  processNameLOCK="${5}"
  processLogLOCK="${6}"
  fileMaxLOCK=$7

  touch "${processLogLOCK}"

  for acceptedFileLOCK in ${acceptedFilesLOCK}; do
    FILES="${inputDirectoryLOCK}/*.${acceptedFileLOCK}"
    for file2rename in $FILES; do
      lineNumberLOCK=$(getLineNumberOfFile "${processLogLOCK}")
      echo "Il y a ${lineNumberLOCK} fichiers dans le log"
      if [ "${file2rename}" != "${FILES}" ] && [[ $lineNumberLOCK < $fileMaxLOCK ]]; then
        echo "Ficher présent : ${file2rename}"
        sendCurl "${processNameLOCK}" "${file2rename}" "Fichier detecte"
        echo "Le fichier detecté est   : ${file2rename}"
        fileExtension=$(getExt "${file2rename}" "1")
        echo "L'extension detecté est  : ${fileExtension}"
        file2rename=$(addExt "${file2rename}" "${newExtensionLOCK}")
        echo "Le fichier est devenu    : ${file2rename}"
        ((id += 1))
        echo "Number of file processed : $id"
        sendCurl "${processNameLOCK}" "${file2rename}" "Fichier verouille"
        filename=$(getFilename "${file2rename}" "0")
        mv -v "${file2rename}" "${outputDirectoryLOCK}/${filename}"
        file2rename="${outputDirectoryLOCK}/${filename}"
        echo "Le fichier est           : ${file2rename}"
        [[ -n "$file2rename" ]] && echo "$file2rename" >> "${processLogLOCK}"
      else
        echo "No compatible file detected for ${acceptedFileLOCK}"
      fi
      espace
    done
  done
}
