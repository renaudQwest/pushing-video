#!/bin/bash

hls() {
  #Necessaire même sous Linux
  export LC_NUMERIC="en_US.UTF-8"

  set -e

  # Usage create-vod-hls.sh SOURCE_FILE [OUTPUT_NAME]
  [[ ! "${1}" ]] && echo "Usage: create-vod-hls.sh SOURCE_FILE [OUTPUT_NAME]" && exit 1

  process="${1}"
  source="${2}"
  target="${3}"

  echo "Source : ${source}"
  echo "Target : ${target}"

  # shellcheck disable=SC2207
  render=($(jq <config.json -r ".directoryActions[] | select(.sourceFolder == \"${process}\").variables.HLSParameters.render[]"))
  renderLength=${#render}
  renderLength=$((renderLength / 3))

  echo "${render[@]}"
  echo "Render Length : ${renderLength}"

  segment_target_duration=4     # try to create a new segment every X seconds
  max_bitrate_ratio=1.07        # maximum accepted bitrate fluctuations
  rate_monitor_buffer_ratio=1.5 # maximum buffer size between bitrate conformance checks

  #########################################################################

  if [[ ! "${target}" ]]; then
    target="${source##*/}" # leave only last component of path
    target="${target%.*}"  # strip extension
  fi
  mkdir -p ${target}

  # shellcheck disable=SC2046
  key_frames_interval="$(echo $(ffprobe ${source} 2>&1 | grep -oE '[[:digit:]]+(.[[:digit:]]+)? fps' | grep -oE '[[:digit:]]+(.[[:digit:]]+)?')*2 | bc || echo '')"
  key_frames_interval=${key_frames_interval:-50}
  # shellcheck disable=SC2046
  key_frames_interval=$(echo $(printf "%.1f\n" $(bc -l <<<"$key_frames_interval/10"))*10 | bc) # round
  key_frames_interval=${key_frames_interval%.*}                                                # truncate to integer

  # static parameters that are similar for all renders
  videoCodec=$(jq ".directoryActions[] | select(.sourceFolder == \"${process}\").variables.HLSParameters.videoCodec" --raw-output "./config.json")
  audioCodec=$(jq ".directoryActions[] | select(.sourceFolder == \"${process}\").variables.HLSParameters.audioCodec" --raw-output "./config.json")

  static_params="-c:a ${audioCodec} -ar 48000 -c:v ${videoCodec} -profile:v main -crf 20 -sc_threshold 0"
  static_params+=" -g ${key_frames_interval} -keyint_min ${key_frames_interval} -hls_time ${segment_target_duration}"
  static_params+=" -hls_playlist_type vod"

  # misc params
  misc_params="-hide_banner -y"

  master_playlist="#EXTM3U
  #EXT-X-VERSION:3
  "
  cmd=""

  echo "Loop starting"

  for render in "${render[@]}"; do

    # render fields
    resolution="$(echo ${render} | cut -d ',' -f 1)"
    bitrate="$(echo ${render} | cut -d ',' -f 2)"
    audiorate="$(echo ${render} | cut -d ',' -f 3)"

    echo "Resolution : ${resolution}"
    echo "Bitrate    : ${bitrate}"
    echo "Audiorate  : ${audiorate}"

    # calculated fields
    width="$(echo ${resolution} | grep -oE '^[[:digit:]]+')"
    height="$(echo ${resolution} | grep -oE '[[:digit:]]+$')"
    maxrate="$(echo "$(echo ${bitrate} | grep -oE '[[:digit:]]+')*${max_bitrate_ratio}" | bc)"
    bufsize="$(echo "$(echo ${bitrate} | grep -oE '[[:digit:]]+')*${rate_monitor_buffer_ratio}" | bc)"
    bandwidth="$(echo ${bitrate} | grep -oE '[[:digit:]]+')000"
    namex="${height}p"

    cmd+=" ${static_params} -vf scale=w=${width}:h=${height}:force_original_aspect_ratio=decrease"
    cmd+=" -b:v ${bitrate} -maxrate ${maxrate%.*}k -bufsize ${bufsize%.*}k -b:a ${audiorate}"
    cmd+=" -hls_segment_filename ${target}/${namex}_%03d.ts ${target}/${namex}.m3u8"

    # add render entry in the master playlist
    master_playlist+="#EXT-X-STREAM-INF:BANDWIDTH=${bandwidth},RESOLUTION=${resolution}\n${namex}.m3u8\n"
  done

  # start conversion
  echo -e "Executing command:\nffmpeg ${misc_params} -i ${source} ${cmd}"
  ffmpeg ${misc_params} -i ${source} ${cmd}

  # create master playlist file
  echo -e "${master_playlist}" >${target}/playlist.m3u8

  echo "Done - encoded HLS is at ${target}/"
}
