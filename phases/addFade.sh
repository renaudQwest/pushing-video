addFade() {
  echo "Input File            : $1, "
  echo "Output File           : $2, "
  echo "Fade in duration      : $3, "
  echo "Fade out duration     : $4, "
  echo "Black screen duration : $5, "
  echo "Codec Video               : $6, "
  echo "Video Bitrate             : $7, "
  echo "Audio Codec               : $8, "
  echo "Audio Bitrate             : $9, "
  echo "Audio Channel             : ${10}, "
  echo "Audio Sampling Frequency  : ${11}"

  sourceFile=${1}
  destinationFile=${2}
  fadeIn=${3}
  fadeOut=${4}
  blackScreen=${5}
  videoCodec=${6}
  videoBitrate=${7}
  audioCodec=${8}
  audioBitrate=${9}
  audioChannel=${10}
  audioSamplingFrequency=${11}

  videoLength=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "${sourceFile}")
  fadeOutFrom=$(echo "$videoLength - $fadeOut" | bc)
  ffmpeg -nostdin -hide_banner -loglevel error -n -i "${sourceFile}" -c:v "${videoCodec}" -b:v "${videoBitrate}"M -minrate "${videoBitrate}"M -maxrate "${videoBitrate}"M -bufsize "${videoBitrate}"M -vf "fade=t=in:st=0:d=$fadeIn,fade=t=out:st=$fadeOutFrom:d=$fadeOut,tpad=stop_duration=${blackScreen},setdar=dar=16/9,setsar=sar=1" -af "afade=t=in:st=0:d=$fadeIn,afade=t=out:st=$fadeOutFrom:d=$fadeOut" -acodec "${audioCodec}" -b:a "${audioBitrate}"k -ac "${audioChannel}" -ar "${audioSamplingFrequency}"k -video_track_timescale 24000 "${destinationFile}"
}
