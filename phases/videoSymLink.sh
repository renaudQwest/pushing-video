videoSymLink() {
  originalPath=${1}
  oldRoot=${2}
  newRoot=${3}

  #originalPath="/Volumes/production/1588/1588-20191114_web_Hermeto_Pascoal_Grupo_111011.mp4"
  #oldRoot="/Volumes/production/"
  #newRoot="/home/rendu/nas/prod/"

  if [[ ${originalPath} == */PRO_* ]];then
    newPath="${originalPath/$oldRoot/$newRoot}"
    filenameSL="${newPath##*/}"
    pathSL="${newPath%/*}"
    echo "${pathSL}/output/${filenameSL}"
    exit 0
  fi

  echo "${originalPath/$oldRoot/$newRoot}"
}

## Test Command to ignore
#videoSymLink "/Volumes/production/1588/PRO_1588-20191114_web_Hermeto_Pascoal_Grupo_111011.mp4" "/Volumes/production/" "/home/rendu/nas/prod/"