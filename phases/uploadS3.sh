uploadS3() {
  file="${1}"
  bucket="${2}"
  folder="${3}"
  filename="${4}"
  profile="${5}"
  #echo "Voici les parametres passés dans s3"
  #echo "${file}"
  #echo "s3://${bucket}/${folder}/${filename}"
  #echo "${profile}"
  aws s3 cp "${file}" "s3://${bucket}/${folder}/${filename}" --profile "${profile}"
}