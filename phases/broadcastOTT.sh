broadcastOTT() {
  file="${1}"
  bucket="${2}"
  folder="${3}"
  filename="${4}"
  profile="${5}"
  echo "Voici les parametres passés dans broadcastOTT"
  echo "${file}"
  echo "s3://${bucket}/${folder}/${filename}"
  aws s3 cp "${file}" "s3://${bucket}/${folder}/${filename}" --profile "${profile}"
}