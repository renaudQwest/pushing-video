for i in *.mp4;
do name=$(echo "$i" | cut -d'.' -f1)
  echo "$name"
  ffmpeg -i "./${i}" -c:v h264 -b:v 10M -force_key_frames "expr:gte(t,n_forced*1)" -acodec aac -b:a 364k -vf "yadif,scale=$6:-2" -pix_fmt yuv420p "/Volumes/AZ00329001//Mai/${name}.mp4"
done
