source ./phases/importColors.sh importColors
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur

createShort() {

  echo "Input File            : $1, "
  echo "Output File           : $2, "
  echo "Timestamp file        : $3, "
  echo "Destination           : $4, "
  echo "Codec Video               : $5, "
  echo "Video Bitrate             : $6, "
  echo "Audio Codec               : $7, "
  echo "Audio Bitrate             : $8, "
  echo "Audio Channel             : $9, "
  echo "Audio Sampling Frequency  : ${10}"

  file2short=${1}
  output=${2}
  timestampsFile=${3}
  destination=${4}
  videoCodec=${5}
  videoBitrate=${6}
  audioCodec=${7}
  audioBitrate=${8}
  audioChannel=${9}
  audioSamplingFrequency=${10}

  importColors

  filename=$(getFilename "${file2short}" "1")
  IFS='-' read -r fileID fileRealName <<< "$filename"
  echo "File Real Name : ${fileRealName}"
  echo "File ID : ${fileID}"

  fileExt=$(getExt "${file2short}" "1")

  echo "File      : ${file2short}"
  echo "Filename  : ${filename}"
  echo "File ext  : ${fileExt}"
  echo "Time file : ${timestampsFile}"

  if [ ! -f "${timestampsFile}" ];then
    >&2 echo "No timestamp file"
    return 3
  else
    echo "Found the timestamp file :D"
  fi

  num=0

  while IFS= read -r line || [[ -n "$line" ]]; do
    if [[ $line =~ [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9],[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9]$ ]]; then
      num=$((num+1))
      espace
      echo "Line number        : $num"
      echo "Text read from file: $line"
      timeIn=$(echo "${line}" | tr , \\n | head -n 1)
      timeOut=$(echo "${line}" | tr , \\n | tail -n 1)
      echo "Time IN  : ${timeIn}"
      echo "Time OUT : ${timeOut}"

      declare "timeIn_${num}=$(echo "${line}" | tr , \\n | head -n 1)"
      declare "timeOut_${num}=$(echo "${line}" | tr , \\n | tail -n 1)"
    fi
  done < "${timestampsFile}"

  separateur
  echo "Number of shorts : $num"

  if [[ ${num} == 0 ]];then
    exit 1
  fi

  for (( i=1; i<="${num}"; i++ )) ; do
    echo "Num : ${i}"

    var1="timeIn_${i}"
    var2="timeOut_${i}"
    echo "Dime IN  : ${!var1}"
    echo "Dime OUT : ${!var2}"

    if [[ $filename == "PRO_"* ]]; then
      echo "Shorting PRO file"
      echo "File final name : ${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}"
      # -codec copy
      ffmpeg -nostdin -hide_banner -loglevel error -i "${file2short}" -ss "${!var1}" -to "${!var2}" -c:v "${videoCodec}" -b:v "${videoBitrate}"M -acodec "${audioCodec}" -b:a "${audioBitrate}"k -ac "${audioChannel}" -ar "${audioSamplingFrequency}"k -y "${output}/${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}"
      mv -v "${output}/${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}" "${destination}/${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}"
    else
      echo "Shorting MASTER file"
      echo "File final name : PRO_${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}"
      # -codec copy
      ffmpeg -nostdin -hide_banner -loglevel error -i "${file2short}" -ss "${!var1}" -to "${!var2}" -c:v "${videoCodec}" -b:v "${videoBitrate}"M -acodec "${audioCodec}" -b:a "${audioBitrate}"k -ac "${audioChannel}" -ar "${audioSamplingFrequency}"k -y "${output}/PRO_${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}"
      mv -v "${output}/PRO_${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}" "${destination}/PRO_${fileID}#$(printf "%02d" ${i})-${fileRealName}.${fileExt}"
    fi
  done
}