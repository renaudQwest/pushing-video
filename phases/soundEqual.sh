soundEqual() {
  I=${1}
  LRA=${2}
  tp=${3}
  inputFile=${4}
  m_I=${5}
  m_LRA=${6}
  m_tp=${7}
  m_thresh=${8}
  t_offset=${9}
  outputFile=${10}
  ac=${11}
  ar=${12}

  ffmpeg -i "${inputFile}" -af "loudnorm=print_format=summary:linear=true:I=${I}:LRA=${LRA}:tp=${tp}:measured_I=${m_I}:measured_LRA=${m_LRA}:measured_tp=${m_tp}:measured_thresh=${m_thresh}:offset=${t_offset}" -ar "${ar}k" -ac "${ac}" "${outputFile}"
}