# 1=file 2=extension
addExt() {
  fiile="${1}"
  fiile="${fiile}.${2}"
  mv "${1}" "${fiile}"
  echo "${fiile}"
}

# 1=file 2=extension
replaceExt() {
  fiile="${1}"
  fiile="${fiile%.*}.${2}"
  mv "${1}" "${fiile}"
  echo "${fiile}"
}

# 1=file
cutExt() {
  fiile="${1}"
  fiile=${fiile%.*}
  mv "${1}" "${fiile}"
  echo "${fiile}"
}

# 1=file 2=position from the last
getExt() {
  echo "${1}" | tr . \\n | tail -n "${2}" | head -n 1
}

# 1=file 2=extension number
getFilename() {
  filename='error'
  filename=${1##*/}
  for ((i = 0; i < $2; i++)); do
    filename=${filename%.*}
  done
  echo "${filename}"
}

# 1=file
cutDirectory() {
  filename=$(echo "${1}" | tr / \\n | tail -n1)
  echo "${filename}"
}

whatFile() {
  echo "What is the file ?"
  echo "The file is ${file}"
  printf "\\n"
}

espace() {
  printf "\\n"
}

separateur() {
  printf "\\n"
  printf "<-------------------------------------->\\n"
  printf "\\n"
}

printEnd() {
  espace
  echo -e "${LIGHTCYAN}                                                                                   *****   *   *   **** "
  echo "                                                                                   *       **  *   *   *"
  echo " <-------------------------------------------------------------------------------- ***     * * *   *   * --------------------------------------------------------------------------------> "
  echo "                                                                                   *       *  **   *   *"
  echo "                                                                                   *****   *   *   **** "
}

getFilePrefix() {
  filename=$(echo "${1}" | tr / \\n | tail -n1)
  IFS='-' read -r fileId trash <<< "$filename" ## Trash is unused
  if [[ $fileId == *@* ]]; then # Remove trailer mention from detection
    IFS='@' read -r fileId trash <<< "$fileId" ## Trash is unused
  fi
  echo "${fileId}"
}

displayError() {
  echo -e "$1"
}

getLineNumberOfFile() {
  l=$(grep -c . "${1}")
  echo "${l}"
}