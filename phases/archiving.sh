# TODO change directory and file for final version (encoded file was used for small sending)
prepareFileToUpload() {
  fileToUpload="${dir_archive}/${id}_backup_${archiveNumber}.$(getFileExtension "${file}")"

  mkdir -p "${dir_publish}/${id}"
  chunks="${dir_publish}/${id}/chunk"

  # TODO Change arguments if file is superior to 500Mo > splittting may be 8Mo for example
  # Splitting byte code file chunked one mega octets
  if [ "$(wc -c <"${file}")" -le 67108864 ]; then
    sizePart=1048576
    split -b ${sizePart} "${fileToUpload}" "${chunks}"
  fi

  if [ "$(wc -c <"${file}")" -gt 67108864 ] && [ "$(wc -c <"${file}")" -le 17179869184 ]; then
    sizePart=33554432
    split -b ${sizePart} "${fileToUpload}" "${chunks}"
  fi

  if [ "$(wc -c <"${file}")" -gt 17179869184 ] && [ "$(wc -c <"${file}")" -le 34359738368 ]; then
    sizePart=67108864
    echo $sizePart
    split -b ${sizePart} "${fileToUpload}" "${chunks}"
  fi

  if [ "$(wc -c <"${file}")" -gt 34359738368 ] && [ "$(wc -c <"${file}")" -le 68719476736 ]; then
    sizePart=134217728
    echo $sizePart
    split -b ${sizePart} "${fileToUpload}" "${chunks}"
  fi

  if [ "$(wc -c <"${file}")" -ge 68719476736 ]; then
    sizePart=268435456
    echo $sizePart
    split -b ${sizePart} "${fileToUpload}" "${chunks}"
  fi
}

initializeUpload() {
  jsonFile="${dir_publish}/${id}_${filename}_QWEST_${date}.json"
  aws glacier initiate-multipart-upload --account-id "${aws_account_id}" --part-size "${sizePart}" --vault-name "${aws_archive_vault_name}" --profile TEST >"${jsonFile}"

  # Extract upload id
  uploadId="$(cat "${jsonFile}" | sed -n 's|.*"uploadId": "\([^"]*\)".*|\1|p')"
  echo "upload: ${uploadId}"

  # Treehash form the encoded file
  treehash="$(treehash "${dir_archive}/${id}_backup_${archiveNumber}.$(getFileExtension "${file}")")"
  echo "Treehash: ${treehash}"
}

# $1 byteEnd $2 totalSize
completedUploadTaskmessage() {
  echo $(($1 * 100 / $2))'% sended |' $(($1 + 1)) bytes / $2 bytes
}

uploadChunks() {
  currentBytes=0
  byteStart=${currentBytes}
  totalSize=$(wc -c <"${dir_archive}/${id}_backup_${archiveNumber}.$(getFileExtension "${file}")" | xargs)

  # Sending all chunks one by one with calculation part
  echo "${dir_publish}/${id}"
  echo 'aws region' "${aws_region_id}" 'aws account' "${aws_account_id}"

  for chunkName in "${dir_publish}/${id}/"*; do
    chunkSize=$(wc -c <"${chunkName}" | xargs)

    if [ "${chunkSize}" -eq ${sizePart} ]; then
      byteEnd=$((byteStart + (chunkSize - 1)));
      aws glacier upload-multipart-part --upload-id "${uploadId}" --body "${chunkName}" --range "bytes ${byteStart}-${byteEnd}/*" --account-id "${aws_account_id}" --vault-name "${aws_archive_vault_name}" --region "${aws_region_id}" --profile TEST >> "${dir_input}/${id}_command.txt"
      ((currentBytes += $((byteEnd - byteStart + 1))));
      completedUploadTaskmessage "${byteEnd}" "${totalSize}";
      byteStart=$((byteEnd + 1));
    else
      byteEnd=$((byteStart + (chunkSize - 1)));
      aws glacier upload-multipart-part --upload-id "${uploadId}" --body "${chunkName}" --range "bytes ${byteStart}-${byteEnd}/*" --account-id "${aws_account_id}" --vault-name "${aws_archive_vault_name}" --region "${aws_region_id}" --profile TEST >> "${dir_input}/${id}_command.txt"
      ((currentBytes += $((byteEnd - byteStart))));
      completedUploadTaskmessage "${byteEnd}"  "${totalSize}"
    fi
  done

  size=$((currentBytes + 1))
  echo "totalSize: " $size
}

uploadHash() {
  aws glacier complete-multipart-upload --checksum "${treehash}" --archive-size "${size}" --upload-id "${uploadId}" --account-id "${aws_account_id}" --vault-name "${aws_archive_vault_name}" --profile TEST >> "${dir_input}/${id}_command.txt"
}

checkingStatus() {
  aws glacier describe-vault --account-id "${aws_account_id}" --vault-name "${aws_archive_vault_name}" --profile TEST >>"${dir_input}/${id}_command.txt"
}
