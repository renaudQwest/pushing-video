source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur

safeFTP() {
  #local_path=/Users/alexandrebenali/INPUT/test_10s.mov
  #remote_path=/home/alexandre/test-sftp

  serverName="${1}"
  processName="${2}"
  FILE="${3}"
  TYPE="${4}"

  LOGIN=$(jq ".serverLogin[] | select(.name == \"${serverName}\").user" --raw-output "./config.json")
  DEST_SERVER=$(jq ".serverLogin[] | select(.name == \"${serverName}\").serverIp" --raw-output "./config.json")
  PORT=$(jq ".serverLogin[] | select(.name == \"${serverName}\").port" --raw-output "./config.json")

  DEST_PATH=$(jq ".directoryActions[] | select(.name == \"${processName}\").serverFolder" --raw-output "./config.json")

  FILENAME=$(getFilename "${FILE}")

  echo "Server Name   : ${serverName}"
  echo "Script Name   : ${processName}"
  echo "SOURCE FOLDER : ${SOURCE_FOLDER}"
  echo "FILE          : ${FILE}"
  echo "TYPE          : ${TYPE}"
  echo "FILENAME      : ${FILENAME}"
  echo "LOGIN         : ${LOGIN}"
  echo "DEST_SERVER   : ${DEST_SERVER}"
  echo "DEST_PATH     : ${DEST_PATH}"
  echo "PORT          : ${PORT}"

if [[ -d ${FILE} ]]; then
  echo "${FILE} is a directory"
  sftp -r "${LOGIN}@${DEST_SERVER}" <<EOF
    cd ${DEST_PATH}
    mkdir ${FILENAME}
    put ${FILE}
EOF
elif [[ -f ${FILE} ]]; then
  echo "${FILE} is a file"
  sftp -r "${LOGIN}@${DEST_SERVER}" <<EOF
    cd ${DEST_PATH}
    put ${FILE}
EOF
else
  echo "${FILE} is not valid"
  exit 1
fi
}

## Test commands
#safeFTP "default" "HD16/9" "/Users/alexandrebenali/INPUT/JAZZ/OUTPUT_HLS/test_10s"
#safeFTP "default" "HD16/9" "/Users/alexandrebenali/INPUT/test_10s.mov"