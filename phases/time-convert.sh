source ./phases/importColors.sh importColors
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur

timeConvert() {

  file=${1}

  importColors

  videoLength=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "${sourceFile}")

  startTime="1970-01-01 ${1}"
  stopTime="1970-01-01 ${2}"

  echo "Start Time  : ${startTime}"
  echo "Stop Time   : ${stopTime}"

  #var1="1970-01-01 23:30:00.456"
  #var2="1970-01-01 20:00:00.123"
  #echo "${var1} et ${var2}"

  startTimeSeconds="$(gdate +'%s' -u -d "${startTime}")"
  stopTimeSeconds="$(gdate +'%s' -u -d "${stopTime}")"

  echo "Start Time  : ${startTimeSeconds}"
  echo "Stop Time   : ${stopTimeSeconds}"

  if [ "${startTimeSeconds}" -gt "${stopTimeSeconds}" ]; then
    echo -e "${RED}WARNING${NOCOLOR} : Start time bigger than stop time !"
    # Get the short duration
    durationSeconds=$((startTimeSeconds - stopTimeSeconds))

    # Get the long duration (stop one day later)
    #stopTimeSeconds=$((stopTimeSeconds + 86400))
    #durationSeconds=$((stopTimeSeconds - startTimeSeconds))
  else
    durationSeconds=$((stopTimeSeconds - startTimeSeconds))
  fi

  echo "Duration    : ${durationSeconds}"

  #h1="$(gdate +'%s' -u -d "${var1}")"
  #h2="$(gdate +'%s' -u -d "${var2}")"
  #h3=$((h1-h2))
  #echo "${h1} - ${h2} = ${h3}"

  duration=$(gdate -u -d @"${durationSeconds}" +"%T")

  echo "Duration    : ${duration}"

  #l1="$(gdate -u -d @"${h1}" +"%T")"
  #echo "${h1}"
  #echo "${l1}"
  #l2="$(gdate -u -d @"${h2}" +"%T")"
  #echo "${h2}"
  #echo "${l2}"
  #l3="$(gdate -u -d @"${h3}" +"%T")"
  #echo "${h3}"
  #echo "${l3}"

  #echo "${l1} - ${l2} = ${l3}"
}

timeConvert "02:00:00" "01:00:00"
