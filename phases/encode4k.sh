#!/bin/bash

source ./phases/importColors.sh importColors
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/cutting.sh cutting

encoding4k() {
  echo "File status               : ${10}"
  echo "(0 : default)"
  echo "(1 : pad)"
  echo "Input File                : $1, "
  echo "Codec Video               : $2, "
  echo "Video Bitrate             : $3, "
  echo "Audio Codec               : $4, "
  echo "Audio Bitrate             : $5, "
  echo "Video Filter              : $6, "
  echo "Audio Channel             : $7, "
  echo "Audio Sampling Frequency  : $8, "
  echo "Output File               : $9, "
  echo "Timestamp File            : ${11}"
  importColors

  num=0
  if [[ -f "${11}" ]]; then
    echo "Timestamp exists"
    while IFS= read -r line || [[ -n "$line" ]]; do
      if [[ $line =~ [0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9],[0-9][0-9]:[0-9][0-9]:[0-9][0-9].[0-9][0-9]$ ]]; then
        num=$((num+1))
        espace
        echo "Line number        : $num"
        echo "Text read from file: $line"
        timeIn=$(echo "${line}" | tr , \\n | head -n 1)
        timeOut=$(echo "${line}" | tr , \\n | tail -n 1)
        echo "Time IN  : ${timeIn}"
        echo "Time OUT : ${timeOut}"

        declare "timeIn_${num}=$(echo "${line}" | tr , \\n | head -n 1)"
        declare "timeOut_${num}=$(echo "${line}" | tr , \\n | tail -n 1)"
      fi
    done < "${11}"

    separateur
    echo "Number of shorts : $num"

    for (( i=1; i<="${num}"; i++ )) ; do
      echo "Num : ${i}"

      var1="timeIn_${i}"
      var2="timeOut_${i}"
      echo "Dime IN  : ${!var1}"
      echo "Dime OUT : ${!var2}"

      if [ "${10}" == "0" ]; then
        ffmpeg -i "$1" -c:v "$2" -ss "${!var1}" -to "${!var2}" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "${6}, scale=w=3840:h=2160, setdar=16/9" -aspect 16:9 -timecode "00:00:00:00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
      elif [ "${10}" == "1" ]; then # ih*16/9:ih:(ow-iw)/2:0
        ffmpeg -i "$1" -c:v "$2" -ss "${!var1}" -to "${!var2}" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "${6}, scale=(iw*sar)*min(3840/(iw*sar)\,2160/ih):ih*min(3840/(iw*sar)\,2160/ih), pad=3840:2160:(3840-iw*min(3840/iw\,2160/ih))/2:(2160-ih*min(3840/iw\,2160/ih))/2" -aspect 16:9 -timecode "00:00:00:00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
      fi
    done
  else
    if [ "${10}" == "0" ]; then
      ffmpeg -i "$1" -c:v "$2" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "${6}, scale=w=3840:h=2160, setdar=16/9" -aspect 16:9 -timecode "00:00:00:00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
    elif [ "${10}" == "1" ]; then # ih*16/9:ih:(ow-iw)/2:0
      ffmpeg -i "$1" -c:v "$2" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "${6}, scale=(iw*sar)*min(3840/(iw*sar)\,2160/ih):ih*min(3840/(iw*sar)\,2160/ih), pad=3840:2160:(3840-iw*min(3840/iw\,2160/ih))/2:(2160-ih*min(3840/iw\,2160/ih))/2" -aspect 16:9 -timecode "00:00:00:00" -acodec "$4" -b:a "$5"k -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
    fi
  fi
}

#                      OK                       |    OK   |    OK    |                   OK                      |                                                                                                                          |                       |     OK     |    OK   |OK (null)|  OK   |    OK    |       OK      |     OK
#ffmpeg -i "$1"                                  -c:v "$2" -b:v "$3"M  -force_key_frames "expr:gte(t,n_forced*1)"                                                                                                                                                    -acodec "$4" -b:a "$5"k -vf "$6" -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
#ffmpeg -i /home/rendu/nas/prod/ID/ID-INPUT.mpeg -c:v h264 -b:v 10M    -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, scale=w=3840:h=2160              ,                                       setdar=16/9, fps=fps=25" -timecode "00:00:00:00" -acodec aac  -b:a 320k           -ac 2    -ar 48000 -pix_fmt yuv420p -y ./_QC/PRO_INPUT.mp4
#ffmpeg -i /home/rendu/nas/prod/ID/ID-INPUT.mpeg -c:v h264 -b:v 10M    -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, scale=w=1440:h=2160:flags=lanczos, pad=width:3840:height=2160:x=240:y=0, setdar=16/9, fps=fps=25" -timecode "00:00:00:00" -acodec aac  -b:a 320k           -ac 2    -ar 48000 -pix_fmt yuv420p -y ./_QC/PRO_INPUT.mp4
#ffmpeg -i /home/rendu/nas/prod/ID/ID-INPUT.mpeg -c:v h264 -b:v 10M    -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, scale=w=1350:h=2160:flags=lanczos, pad=width:3840:height=2160:x=240:y=0, setdar=16/9, fps=fps=25" -timecode "00:00:00:00" -acodec aac  -b:a 320k           -ac 2    -ar 48000 -pix_fmt yuv420p -y ./_QC/PRO_INPUT.mp4

#ffmpeg -i "$1" -c:v "$2" -b:v "$3"M -force_key_frames "expr:gte(t,n_forced*1)" -acodec "$4" -b:a "$5"k -vf "$6" -ac "$7" -ar "$8"k -pix_fmt yuv420p -y "$9"
#ffmpeg -i "$1" -c:v h264 -b:v 10M    -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, scale=w=3840:h=2160              ,                                       setdar=16/9, fps=fps=25" -timecode "00:00:00:00" -acodec aac  -b:a 320k           -ac 2    -ar 48000 -pix_fmt yuv420p -y "$9.1.mp4"
#ffmpeg -i "$1" -c:v h264 -b:v 10M    -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, scale=w=1440:h=2160:flags=lanczos, pad=width=3840:height=2160:x=240:y=0, setdar=16/9, fps=fps=25" -timecode "00:00:00:00" -acodec aac  -b:a 320k           -ac 2    -ar 48000 -pix_fmt yuv420p -y "$9.2.mp4"
#ffmpeg -i "$1" -c:v h264 -b:v 10M    -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, scale=w=1350:h=2160:flags=lanczos, pad=width=3840:height=2160:x=240:y=0, setdar=16/9, fps=fps=25" -timecode "00:00:00:00" -acodec aac  -b:a 320k           -ac 2    -ar 48000 -pix_fmt yuv420p -y "$9.3.mp4"
