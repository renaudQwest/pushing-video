remoteSync() {
  PORT=0

  serverName="default"
  serverName="${1}"
  processName="${2}"
  FILE="${3}"

  LOGIN=$(jq ".serverLogin[] | select(.name == \"${serverName}\").user" --raw-output "./config.json")
  DEST_SERVER=$(jq ".serverLogin[] | select(.name == \"${serverName}\").serverIp" --raw-output "./config.json")
  PORT=$(jq ".serverLogin[] | select(.name == \"${serverName}\").port" --raw-output "./config.json")

  DEST_PATH="$(jq ".directoryActions[] | select(.name == \"${processName}\").serverFolder" --raw-output "./config.json")/"

  echo "Server Name   : ${serverName}"
  echo "Script Name   : ${processName}"
  echo "SOURCE FOLDER : ${SOURCE_FOLDER}"
  echo "FILE          : ${FILE}"
  echo "LOGIN         : ${LOGIN}"
  echo "DEST_SERVER   : ${DEST_SERVER}"
  echo "DEST_PATH     : ${DEST_PATH}"
  echo "PORT          : ${PORT}"
  echo "DESTINATION   : ${LOGIN}@${DEST_SERVER}:${DEST_PATH}"
  echo " "

  if [ "${PORT}" != 0 ]; then
    if [[ ${FILE} ]]; then
      echo "Avec port"
      rsync -av "${FILE}" -e "ssh -p ${PORT}" "${LOGIN}@${DEST_SERVER}:${DEST_PATH}"
    else
      echo "${FILE} is not valid"
      exit 1
    fi
  else
    if [[ ${FILE} ]]; then
      echo "Sans port"
      rsync -av "${FILE}" "${LOGIN}@${DEST_SERVER}:${DEST_PATH}"
    else
      echo "${FILE} is not valid"
      exit 1
    fi
  fi
}