## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP

name="${1}"

dir_input="${HOME}/INPUT/${rootFolder}"

source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
destSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").destinationFolder" --raw-output "./config.json")"

acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"

echo "Début upload"
espace

uploaded="${destSource}"
for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  echo "$acceptedFile"
  echo "$FILES"
  for file in $FILES; do
    if [ "${file}" != "${FILES}" ];then
    echo "${file}"
    echo "Starting ${file} upload"

    filename=$(getFilename "${file}" "1")
    echo "Filename : ${filename}"

    secureCopy "cdn" "${name}" "${file}"
    #safeFTP "cdn" "${name}" "${file}"

    echo "The file ${file} has been correctly uploaded"

    mv "${file}" "${uploaded}"
    echo "The file ${file} has been correctly moved"
    fi
  done
done

echo "Upload terminé"
separateur
separateur
separateur
separateur
separateur