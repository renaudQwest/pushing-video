

dir_input="${HOME}/INPUT"

FILE="${dir_input}/TEST-915_SD_4_3_anamorphose.mov"
#                                   ####                                                                    setsar=1, setdar=16/9, pad=1920:1080:-1:-1, yadif=0:-1:0, fps=fps=25,   yadif=0:-1:0, fps=fps=25, setsar=sar=1, setdar=dar=16/9, scale=w=1920:h=1080:force_original_aspect_ratio=decrease
ffmpeg -i "${FILE}" -c:v h264 -b:v 10M -force_key_frames "expr:gte(t,n_forced*1)" -filter:v "yadif=0:-1:0, fps=fps=25, scale=(iw*sar)*min(1920/(iw*sar)\,1080/ih):ih*min(1920/(iw*sar)\,1080/ih), pad=1920:1080:(1920-iw*min(1920/iw\,1080/ih))/2:(1080-ih*min(1920/iw\,1080/ih))/2" -timecode "00:00:00:00" -t "00:00:10" -acodec aac -b:a 320k -ac 2 -ar 48k -pix_fmt yuv420p -y "${dir_input}/SD_16_9_test1.mov"