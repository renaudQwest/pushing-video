## Moving the current working directory for local sources
parent_path=$(
  cd "$(dirname "${BASH_SOURCE[0]}")"
  pwd -P
)
cd "$parent_path"

source ./phases/encode.sh encoding
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur getFilePrefix
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP
source ./phases/sendCurl.sh sendCurl
source ./phases/importColors.sh importColors
source ./phases/createShort.sh createShort
source ./phases/videoSymLink.sh videoSymLink

importColors
## Setting variables
name=SHORTS

dir_input="$(jq ".directoryRoot" --raw-output "./config.json")"

source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
encodingSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoding"
encodedSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoded"
errorFolder="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/error/"
destSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").destinationFolder" --raw-output "./config.json")"
#checkDelay="$(jq ".checkDelay" --raw-output "./config.json")"

acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"

id=0

echo "Le dossier d'entrée est :"
echo "${source}"
espace

echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Debut verrouillage fichiers"
espace

for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  for file in $FILES; do
    ## checkCommand checks if the file is old enough
    #checkCommand="find ${foundFile} -mmin +${checkDelay} -type f"
    #echo "${checkCommand}"
    if [ "${file}" != "${FILES}" ]; then
      #for file in $(eval "${checkCommand}"); do
      sendCurl "${name}" "${file}" "Fichier detecte"
      echo "Le fichier detecté est   : ${file}"
      fileExtension=$(getExt "${file}" "1")
      echo "L'extension detecté est  : ${fileExtension}"
      filename=$(getFilename "${file}" "1")
      echo "Le nom du fichier est    : ${filename}"
      file=$(addExt "${file}" "lock")
      echo "Le fichier est devenu    : ${file}"
      ((id += 1))
      echo "Number of file processed : $id"
      sendCurl "${name}" "${file}" "Fichier verouille"
      #done
    else
      echo "No compatible file detected for ${acceptedFile}"
    fi
    espace
  done
done

echo "Fin verrouillage fichiers"
separateur

#######################################################################################################################

## Taking parameters from config file
#storageRoot=$(jq ".storageRoot" --raw-output "./config.json")
linuxRoot=$(jq ".linuxRoot" --raw-output "./config.json")

videoCodec=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoCodec" --raw-output "./config.json")
videoBitrate=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoBitrate" --raw-output "./config.json")
audioCodec=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioCodec" --raw-output "./config.json")
audioBitrate=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioBitrate" --raw-output "./config.json")
audioChannel=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioChannel" --raw-output "./config.json")
audioSamplingFrequency=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioSamplingFrequency" --raw-output "./config.json")


echo "Début shorting"
espace

DIR="${source}/*"
for file in $DIR; do
  if [[ ${file} == *.lock ]] && [ ${id} != 0 ]; then
    ## Starting message
    status=0
    sendCurl "${name}" "${file}" "Debut de lencodage du fichier"

    ## File lock and identification
    whatFile
    filename=$(getFilename "${file}" "2")
    echo "Filename       : ${filename}"
    fileExtension=$(getExt "${file}" "2")
    echo "File extension : ${fileExtension}"
    mv -v "${file}" "${encodingSource}/${filename}.${fileExtension}"
    file="${encodingSource}/${filename}.${fileExtension}"
    fileID=$(getFilePrefix "${filename}")
    echo "ID : ${fileID}"
    fileID=${fileID#*_}
    echo "ID : ${fileID}"

    ## Taking the real file location instead of the symlink
    ## SHORT DOES NOT USE SYMLINK
    realFile="${file}"
    #realFile=$(videoSymLink "${file}" "${encodingSource}/" "${linuxRoot}/${fileID}/")
    echo "Real file       : ${realFile}"
    echo "Symlink file    : ${file}"

    ## Debug functions
    createShort "${realFile}" "${encodingSource}" "${source}/${filename}.txt" "${destSource}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${audioChannel}" "${audioSamplingFrequency}"

    ## Encoding function
    #output=$(createShort "${realFile}" "${encodingSource}" "${source}/${filename}.txt" "${destSource}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${audioChannel}" "${audioSamplingFrequency}" 2>&1)
    status=$?

    ## Error display
    output=$(echo "${output}" | tail -n1)
    echo "Output                 : ${output}"
    echo "SHORTS encoding status : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      echo "${RED}ERROR WITH SHORTS (code : ${YELLOW}${status}${RED})${NOCOLOR}"
      ## Send Error message and status
      sendCurl "${name}" "${file}" "Fichier non encode"
      sendCurl "${name}" "${file}" "${output}"
      echo "Le fichier ${filename} n'a pas été encodé"
      mv -v "${file}" "${errorFolder}"
    else
      sendCurl "${name}" "${file}" "Fichier encode"
      echo "Le fichier ${filename} a été encodé"
    fi

    ## Moving file after encoding
    echo "File : ${file}"
    echo "Dest : ${encodedSource}/${filename}.${fileExtension}"
    mv -v "${file}" "${encodedSource}/${filename}.${fileExtension}"

  else
    filename=$(getFilename "${file}" "0")
    echo "Le fichier ${filename} n'est pas à encoder"
  fi

  espace
done

echo "Shorting terminé"
separateur
separateur
separateur
separateur
separateur
