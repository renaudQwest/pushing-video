#!/bin/bash
configFile=$(jq '.' "./config.json")
rootFolder=$(jq --raw-output '.directoryRoot' "./config.json")
sourceFolders=$(jq --raw-output '.directoryActions[].sourceFolder' "./config.json")
destinationFolders=$(jq --raw-output '.directoryActions[].destinationFolder' "./config.json")
action=$(jq --raw-output '.directoryActions[].action' "./config.json")

dir_input=$HOME/${rootFolder}

for folder in ${sourceFolders}; do
  mkdir -p "${dir_input}/${folder}"
done

# COLORIZE MESSAGE
red='\033[31m'
green='\033[32m'
orange='\033[33m'
cyan='\033[36m'
lightCyan='\033[37m'
noColor='\033[0m'

# REGULAR EXPRESSION
regexExtension='(?=.)([a-z\d]+$)'
regexFileName='([a-zA-Z\d_]+)(?!=>\.[a-z0-9]{3,4})'

# VARIABLE
qualityType=$1
id=$2
archiveNumber=$3
videoCodec="h264"
videoBitrate=0
audioCodec="aac"
audioBitrate=384
date=$(date '+%y')
sizePart=1048576
width=1920 #hd by default
size=0
exitingMessage="${orange}ctrl + c for exit${noColor}"
uploadId=""
treehash=""

phaseMessage() {
  echo "                                                        "
  echo "********************************************************"
  echo -e $1
  echo "********************************************************"
  echo "                                                        "
}

getFileName() {
  echo "$file" | cut -d'/' -f6 | cut -d'.' -f1
}

getFileExtension() {
  echo "$1" | cut -d'.' -f2
}

simpleMessage() {
  echo -e "$1"
}

errorMessage() {
  echo "${red}$1${noColor}"
}

creatingDirectory() {
  mkdir -p "$dir_input"
  mkdir -p "$dir_archive"
  mkdir -p "$dir_encode"
  mkdir -p "$dir_publish"
}

moveToArchive() {
  cp "${file}" "$dir_input/TO_ARCHIVE/${id}_backup_${archiveNumber}.$(getFileExtension "${file}")"
}

phaseMessage "Starting Watching folder on ${dir_input}"
simpleMessage "${exitingMessage}"

chooseQualityType() {
  echo -n -e "${cyan}Choose Quality type ( Two possible choice: ${noColor}hd ${cyan}or${noColor} 4k${cyan}):${noColor}"
  read -r qualityType
  while [[ ! $qualityType == 'hd' && ! $qualityType == '4k' ]]; do
    echo -e "${red}Invalid choice please choice ${noColor}'hd' ${red}or  ${noColor}'4k'"
    read -r qualityType
  done
}

chooseAnID() {
  limit="(Can be lowercase, uppercase or numbers)"
  echo -n -e "${cyan}Choose id${noColor} $limit${cyan}: ${noColor}"
  read -r id
  while [[ ! $id =~ [a-zA-Z0-9]+ ]]; do
    echo -e "${red}Invalid value please redone${noColor} $limit"
    read -r id
  done
}

chooseAnArchiveNumber() {
  limit="(It's a number by default)"
  echo -n -e "${cyan}Choose archive's number${noColor} $limit${cyan}: ${noColor}"
  read -r archiveNumber
  while [[ ! $archiveNumber =~ [0-9]+ ]]; do
    echo -e "${red}Invalid value please redone${noColor} $limit"
    read -r archiveNumber
  done
}

changeParameter() {
  chooseQualityType
  chooseAnID
  chooseAnArchiveNumber
}

# ================
# Starting runtime
# ================

if [ ! "$1" = "hd" ] && [ ! "$1" = "4k" ]; then
  errorMessage "Missing Arguments: Quality ${noColor}'hd' ${red}or ${noColor}'4k'${red} in ${noColor}lowercase"
  chooseQualityType
fi

case $qualityType in
'hd')
  videoCodec='h264'
  videoBitrate=10
  width=1920
  ;;
'4k')
  videoCodec='h264'
  videoBitrate=20
  width=3840
  ;;
esac

if [ "$2" = "" ]; then
  errorMessage "Missing Arguments: Identifier (It's a number by default)"
  chooseAnID
fi

if [ "$3" = "" ]; then
  errorMessage "Missing Arguments: number of archive for AWS Glacier (It's a number by default)"
  chooseAnArchiveNumber
fi

# Creating directories and add permissions
creatingDirectory
chmod -R 777 "${dir_input}"

fswatch "$dir_input" -x './' flag Created | while read -r file event; do

  ## Event target created and is a file
  if [[ $event =~ 'Created PlatformSpecific OwnerModified IsFile' ]]; then
    phaseMessage "${red}Starting phase${noColor}"
    simpleMessage "${lightCyan}Copying archive${noColor}"
    moveToArchive
    simpleMessage "${green}End of starting phase !${noColor}"

    phaseMessage "${red}Encoding${noColor}"
    filename="$(getFileName "${file}")"
    encoding "${file}" ${videoCodec} ${videoBitrate} ${audioCodec} ${audioBitrate} "${dir_encode}/${id}_${filename}_QWEST_${date}.mp4"
    simpleMessage "${green}End of encoding phase !${noColor}"

    phaseMessage "${red}Uploading Media${noColor}"
    uploadToBroadcastSVOD
    simpleMessage "${green}End of uploading media phase !${noColor}"

    phaseMessage "${red}Preparing to archiving in glacier${noColor}"
    simpleMessage "${lightCyan}Prepare File to upload${noColor}"
    prepareFileToUpload
    simpleMessage "${lightCyan}Initializing upload${noColor}"
    initializeUpload
    simpleMessage "${lightCyan}Uploading chunk${noColor}"
    uploadChunks
    simpleMessage "${lightCyan}Uploading hashtree${noColor}"
    uploadHash
    simpleMessage "${lightCyan}Checking status${noColor}"
    checkingStatus
    simpleMessage "${green}End of archiving${noColor}"
  fi
done

for folder in ${destinationFolders}; do
  mkdir -p "${dir_input}/${folder}"
done

