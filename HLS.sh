## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

source ./phases/create-vod-hls.sh hls
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP
source ./phases/remoteSync.sh remoteSync
source ./phases/sendCurl.sh sendCurl

## Setting variables
name=HLS

dir_input="${HOME}/INPUT/${rootFolder}"

source="${dir_input}/$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").sourceFolder" --raw-output "./config.json")"
destSource="${dir_input}/$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").destinationFolder" --raw-output "./config.json")"

acceptedFiles="$(jq ".directoryActions[] | select(.sourceFolder == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"

id=0

echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Début verrouillage fichiers"
espace

for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  for file in $FILES; do
    if [ "${file}" != "${FILES}" ]; then
      sendCurl "${name}" "${file}" "Fichier détecté"
      echo "Le fichier detecté est   : ${file}"
      fileExtension=$(getExt "${file}" "1")
      echo "L'extension detecté est  : ${fileExtension}"
      file=$(addExt "${file}" "lock")
      echo "Le fichier est devenu    : ${file}"
      ((id += 1))
      echo "Number of file processed : $id"
      sendCurl "${name}" "${file}" "Fichier verouillé"
    else
      echo "No compatible ${acceptedFile} file detected"
    fi
    espace
  done
done

echo "Fin verrouillage fichiers"
separateur

#######################################################################################################################

echo "Début HLS"
espace

DIR="${source}/*"
for file in $DIR; do
  if [[ ${file} == *.lock ]] && [ ${id} != 0 ]; then
     ## Starting message
    status=0
    sendCurl "${name}" "${file}" "Début de l'encodage du fichier"

    ## File lock and identification
    file=$(replaceExt "${file}" "locked")
    whatFile
    filename=$(getFilename "${file}" "2")
    echo "Filename : ${filename}"
    echo "Input  : ${file}"
    echo "Output : ${destSource}/${filename}"

    ## Encoding function
    output=$(hls "${name}" "${file}" "${destSource}/${filename}" 2>&1)
    status=$?

    ## Error display
    output=$(echo "${output}" | tail -n1)
    echo "Output              : ${output}"
    echo "HLS encoding status : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      echo "ERROR WITH HLS (code : ${status})"
      ## Remove temporary folder if an error occurred
      rm -rf "${destSource:?}/${filename}"
      ## Send Error message and status
      sendCurl "${name}" "${file}" "Fichier non encodé"
      sendCurl "${name}" "${file}" "${output})"
      echo "Le fichier ${filename} n'a pas été HLS"
    else
      sendCurl "${name}" "${file}" "Fichier encodé"
      echo "Le fichier ${filename} a été HLS"
    fi

    espace
  else
    echo "Pas de fichiers à HLS"
    espace
  fi
done

echo "HLS terminé"
separateur

echo "Début upload"
espace

DEST="${destSource}/*"
uploaded="${destSource}/uploaded"
for folder in $DEST; do
  #Variables
  folderName=$(getFilename "${file}" "1")
  status=0

  ## Check if file is not the container folder
  if [[ ${folder} != "${uploaded}" ]] && [ ${id} != 0 ]; then
    ## Starting message
    echo "Dossier : ${folderName}"
    echo "Starting ${folderName} upload"

    #Upload functions
    #output=$(secureCopy "cdn" "${name}" "${folder}" "${rootFolder}" 2>&1)
    output=$(remoteSync "cdn" "${name}" "${folder}" "${rootFolder}" 2>&1)
    #output=$(safeFTP "cdn" "${name}" "${folder}" "${rootFolder}" 2>&1)

    ## Error display
    status=$?
    output=$(echo "${output}" | tail -n1)
    echo "Output            : ${output}"
    echo "HLS upload status : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      ## Send Error message and status
      sendCurl "${name}" "${folder}" "Fichier non uploadé"
      sendCurl "${name}" "${folder}" "${output}"
      echo "ERROR WITH THE UPLOAD (code : ${status})"
      echo "The directory ${folder} has NOT been correctly uploaded !!!"
    else
      sendCurl "${name}" "${folder}" "Fichier uploadé"
      echo "The directory ${folder} has been correctly uploaded"
      ## Move the folder to not upload it again
      mv "${folder}" "${uploaded}"
      echo "The directory ${folder} has been correctly moved"
    fi
  fi
done

echo "Upload terminé"
separateur
separateur
separateur
separateur
separateur