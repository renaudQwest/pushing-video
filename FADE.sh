## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

## Imports
source ./phases/encoding.sh encoding
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur printEnd displayError
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP
source ./phases/sendCurl.sh sendCurl
source ./phases/importColors.sh importColors
source ./phases/videoSymLink.sh videoSymLink
source ./phases/fileLock.sh fileLock
source ./phases/addFade.sh addFade
source ./checkMachineWorking.sh startMachineWork isMachineWorking stopMachineWork checkMachineWorking
importColors

## Setting variables
name=FADE
echo "Process name : ${name}"

## Checking if the machine is already running another process
checkMachineWorking "${name}"

dir_input="$(jq ".directoryRoot" --raw-output "./config.json")"

source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
encodingSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoding"
encodedSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoded"
errorFolder="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/error/"
destSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").destinationFolder" --raw-output "./config.json")"
acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"
fileAtOnce=$(jq ".fileAtOnce" --raw-output "./config.json")
## Taking system date
unameOut="$(uname -s)"
case "${unameOut}" in
  Linux*)     todayDate=$(date +"%Y-%m-%d_%H-%M-%S");;     # Linux
  Darwin*)    todayDate=$(gdate +"%Y-%m-%d_%H-%M-%S");;    # MacOS
esac
echo "Date du jour     : ${todayDate}"
processLogFolder="${source}/.tmp"
processLogFile="${processLogFolder}/doing_${todayDate}.txt"
echo "Log file : ${processLogFile}"

id=0

echo "Le dossier d'entrée est : ${source}"
echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Debut verrouillage fichiers"
espace

FILES="${processLogFolder}/doin*"
for file in $FILES; do
    if [[ -f $file ]]; then
      # file found, do some stuff and break
      displayError "${RED}Already working, ABORT${NOCOLOR}"
      stopMachineWork
      exit 1
    else
      echo "Free spot"
      echo "Doing usual stuff ..."
      fileLock "${source}" "${source}" "lock" "${acceptedFiles}" "${name}" "${processLogFile}" "${fileAtOnce}"
      separateur
      echo "Checking if log file is empty"
      logContent=$(cat "${processLogFile}")
      echo "Log content : "
      echo "${logContent}"
      if [[ -z "$logContent" ]];then
        rm -f "${processLogFile}"
        echo "Removed log file"
      else
        echo "Files detected ! Continuing process ..."
      fi
      separateur
    fi
done

echo "Fin verrouillage fichiers"
separateur
separateur

#######################################################################################################################

## Taking parameters from config file
fadeIn=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.fadeIn" --raw-output "./config.json")
fadeOut=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.fadeOut" --raw-output "./config.json")
blackScreen=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.blackScreen" --raw-output "./config.json")

videoCodec=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoCodec" --raw-output "./config.json")
videoBitrate=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoBitrate" --raw-output "./config.json")
audioCodec=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioCodec" --raw-output "./config.json")
audioBitrate=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioBitrate" --raw-output "./config.json")
audioChannel=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioChannel" --raw-output "./config.json")
audioSamplingFrequency=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioSamplingFrequency" --raw-output "./config.json")

#storageRoot=$(jq ".storageRoot" --raw-output "./config.json")
linuxRoot=$(jq ".linuxRoot" --raw-output "./config.json")

echo "Début encodage"
espace

while IFS= read -r file || [[ -n "$file" ]]; do
  echo -e "${YELLOW}Text read from file: $file${NOCOLOR}"


  if [[ ${file} == *.lock ]] && [ ${id} != 0 ]; then
    ## Starting message
    status=0
    sendCurl "${name}" "${file}" "Debut de lencodage du fichier"

    ## File lock and identification
    whatFile
    filename=$(getFilename "${file}" "2")
    echo "Filename       : ${filename}"
    fileExtension=$(getExt "${file}" "2")
    echo "File extension : ${fileExtension}"
    mv "${file}" "${encodingSource}/${filename}.${fileExtension}"
    file="${encodingSource}/${filename}.${fileExtension}"
    fileID=$(getFilePrefix "${filename}")
    echo "ID : ${fileID}"
    videoLogFile="${errorFolder}/.log/log-${filename}.txt"
    rm "${videoLogFile}"
    echo "Video log file  : ${videoLogFile}"

    ## Taking the real file location instead of the symlink
    ## FADE DOES NOT USE SYMLINK
    realFile="${file}"
    #realFile=$(videoSymLink "${file}" "${encodingSource}/" "${linuxRoot}/${fileID}/")
    echo "Real file       : ${realFile}"

    ## Encoding function
    addFade "${realFile}" "${destSource}/encoding/${filename}.fade.${fileExtension}" "${fadeIn}" "${fadeOut}" "${blackScreen}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${audioChannel}" "${audioSamplingFrequency}" 2>>"${videoLogFile}"
    status=$?

    ## Error display
    output=$(cat < "${videoLogFile}")
    echo "Output                    : "
    echo "${output}"
    echo "${name} encoding status    : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      echo "${RED}ERROR WITH FADE (code : ${YELLOW}${status}${RED})${NOCOLOR}"
      ## Send Error message and status
      sendCurl "${name}" "${file}" "Fichier non encode"
      sendCurl "${name}" "${file}" "${output}"
      echo "Le fichier ${filename} n'a pas été encodé"
      mv -v "${file}" "${errorFolder}"
    else
      sendCurl "${name}" "${file}" "Fichier encode"
      ## Move encoded file to know it has finished writing
      mv "${destSource}/encoding/${filename}.fade.${fileExtension}" "${destSource}/${filename}_fade.${fileExtension}"
      echo "Le fichier ${filename} a été encodé"
    fi

    ## Moving file after encoding
    mv "${file}" "${encodedSource}/${filename}.${fileExtension}"

  else
    filename=$(getFilename "${file}" "0")
    echo "Le fichier ${filename} n'est pas à encoder"
  fi
  separateur
  espace
done < "${processLogFile}"

mv -v "${processLogFile}" "${source}/.tmp/done_${todayDate}.txt"
stopMachineWork

echo "Encodage terminé"

printEnd
