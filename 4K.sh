## Moving the current working directory for local sources
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

source ./phases/encode4k.sh encoding4k
source ./phases/naming.sh addExt replaceExt cutExt getExt getFilename cutDirectory whatFile espace separateur
source ./phases/secureCopy.sh secureCopy
source ./phases/safeFTP.sh safeFTP
source ./phases/sendCurl.sh sendCurl
source ./phases/importColors.sh importColors
source ./phases/videoSymLink.sh videoSymLink

importColors
## Setting variables
name=${1}
echo "Process name : ${name}"

dir_input="$(jq ".directoryRoot" --raw-output "./config.json")"

source="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")"
encodingSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoding"
encodedSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/encoded"
errorFolder="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/error/"
destSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").destinationFolder" --raw-output "./config.json")"
anaSource="${dir_input}/$(jq ".directoryActions[] | select(.name == \"${name}\").sourceFolder" --raw-output "./config.json")/topad"
#checkDelay="$(jq ".checkDelay" --raw-output "./config.json")"

acceptedFiles="$(jq ".directoryActions[] | select(.name == \"${name}\").acceptedFiles[]" --raw-output "./config.json")"

id=0

echo "Le dossier d'entrée est :"
echo "${source}"
espace

echo "Les extensions acceptées sont :"
echo "${acceptedFiles[*]}"
espace

echo "Debut verrouillage fichiers"
espace

for acceptedFile in ${acceptedFiles}; do
  FILES="${anaSource}/*.${acceptedFile}"
  for file in $FILES; do
    ## checkCommand checks if the file is old enough
    #checkCommand="find ${foundFile} -mmin +${checkDelay} -type f"
    #echo "${checkCommand}"
    if [ "${file}" != "${FILES}" ]; then
      echo "Ficher présent : ${file}"
      #for file in $(eval "${checkCommand}"); do
        sendCurl "${name}" "${file}" "Fichier detecte"
        echo "Le fichier detecté est   : ${file}"
        fileExtension=$(getExt "${file}" "1")
        echo "L'extension detecté est  : ${fileExtension}"
        file=$(addExt "${file}" "topad.lock")
        echo "Le fichier est devenu    : ${file}"
        ((id += 1))
        echo "Number of file processed : $id"
        sendCurl "${name}" "${file}" "Fichier verouille"
        filename=$(getFilename "${file}" "0")
        mv -v "${file}" "${source}/${filename}"
        file="${source}/${filename}"
        echo "Le fichier est situé     : ${file}"
      #done
    else
      echo "[PAD] No compatible file detected for ${acceptedFile}"
    fi
    espace
  done
done

for acceptedFile in ${acceptedFiles}; do
  FILES="${source}/*.${acceptedFile}"
  for file in $FILES; do
    ## checkCommand checks if the file is old enough
    #checkCommand="find ${foundFile} -mmin +${checkDelay} -type f"
    #echo "${checkCommand}"
    if [ "${file}" != "${FILES}" ]; then
      echo "Ficher présent : ${file}"
      #for file in $(eval "${checkCommand}"); do
        sendCurl "${name}" "${file}" "Fichier detecte"
        echo "Le fichier detecté est   : ${file}"
        fileExtension=$(getExt "${file}" "1")
        echo "L'extension detecté est  : ${fileExtension}"
        filename=$(getFilename "${file}" "1")
        echo "Le nom du fichier est    : ${filename}"
        file=$(addExt "${file}" "def.lock")
        echo "Le fichier est devenu    : ${file}"
        ((id += 1))
        echo "Number of file processed : $id"
        sendCurl "${name}" "${file}" "Fichier verouille"
      #done
    else
      echo "No compatible file detected for ${acceptedFile}"
    fi
    espace
  done
done

echo "Fin verrouillage fichiers"
separateur

#######################################################################################################################

## Taking parameters from config file
videoCodec=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoCodec" --raw-output "./config.json")
videoBitrate=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoBitrate" --raw-output "./config.json")
videoFilter=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.videoFilter" --raw-output "./config.json")
audioCodec=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioCodec" --raw-output "./config.json")
audioBitrate=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioBitrate" --raw-output "./config.json")
audioChannel=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioChannel" --raw-output "./config.json")
audioSamplingFrequency=$(jq ".directoryActions[] | select(.name == \"${name}\").variables.transcode.audioSamplingFrequency" --raw-output "./config.json")

#storageRoot=$(jq ".storageRoot" --raw-output "./config.json")
linuxRoot=$(jq ".linuxRoot" --raw-output "./config.json")

echo "Début encodage"
espace

DIR="${source}/*"
for file in $DIR; do
  if [[ ${file} == *.lock ]] && [ ${id} != 0 ]; then
    ## Starting message
    status=0
    sendCurl "${name}" "${file}" "Debut de lencodage du fichier"

    ## File lock and identification
    whatFile
    filename=$(getFilename "${file}" "3")
    echo "Filename        : ${filename}"
    timestamp="${source}/${filename}.txt"
    echo "Timestamp       : ${timestamp}"
    fileExtension=$(getExt "${file}" "3")
    echo "File extension  : ${fileExtension}"
    bonusExtension=$(getExt "${file}" "2")
    echo "Bonus extension : ${bonusExtension}"
    mv -v "${file}" "${encodingSource}/${filename}.${fileExtension}"
    file="${encodingSource}/${filename}.${fileExtension}"
    fileID=${filename%-*}
    for (( i = 0 ; i < 20 ; i += 1)) ; do
      fileID=${fileID%-*}
    done
    echo "ID : ${fileID}"

    ## Taking the real file location instead of the symlink
    realFile="${file}"
    echo "Testing if file is symlink : ${file}"
    fileSize=$(du -k "$file" | cut -f 1)
    echo "Size : ${fileSize}"
    if ((fileSize < 100));then
      echo "Symlink detected, finding real file"
      realFile=$(videoSymLink "${file}" "${encodingSource}/" "${linuxRoot}/${fileID}/")
    else
      echo "No symlink detected, using source file"
    fi
    echo "Real file       : ${realFile}"

    ## Debug functions
    #encoding4k "${file}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${videoFilter}" "${audioChannel}" "${audioSamplingFrequency}" "${destSource}/encoding/${filename}.mp4" || status=$?
    #cp "${file}" "${destSource}/${filename}.mp4"

    ## Encoding function
    if [[ ${bonusExtension} == topad ]]; then
      echo "Encodage pad"
      #encoding4k "${realFile}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${videoFilter}" "${audioChannel}" "${audioSamplingFrequency}" "${destSource}/encoding/PRO_${filename}.mp4" "1" "${timestamp}"
      output=$(encoding4k "${realFile}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${videoFilter}" "${audioChannel}" "${audioSamplingFrequency}" "${destSource}/encoding/PRO_${filename}.mp4" "1" "${timestamp}" 2>&1)
      status=$?
    elif [[ ${bonusExtension} == def ]]; then
      echo "Encodage def"
      #encoding4k "${realFile}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${videoFilter}" "${audioChannel}" "${audioSamplingFrequency}" "${destSource}/encoding/PRO_${filename}.mp4" "0" "${timestamp}"
      output=$(encoding4k "${realFile}" "${videoCodec}" "${videoBitrate}" "${audioCodec}" "${audioBitrate}" "${videoFilter}" "${audioChannel}" "${audioSamplingFrequency}" "${destSource}/encoding/PRO_${filename}.mp4" "0" "${timestamp}" 2>&1)
      status=$?
    else
        echo "Error : no profile detected"
        whatFile
    fi

    ## Error display
    output=$(echo "${output}" | tail -n1)
    echo "Output                 : ${output}"
    echo "4k encoding status     : ${status}"
    espace

    ## Error handling
    if ((status != 0)); then
      echo "ERROR WITH 4K (code : ${status})"
      ## Send Error message and status
      sendCurl "${name}" "${file}" "Fichier non encode"
      sendCurl "${name}" "${file}" "${output}"
      echo "Le fichier ${filename} n'a pas été encodé"
      mv -v "${file}" "${errorFolder}"
    else
      sendCurl "${name}" "${file}" "Fichier encode"
      ## Move encoded file to know it has finished writing
      mv -v "${destSource}/encoding/PRO_${filename}.mp4" "${destSource}/PRO_${filename}.mp4"
      echo "Le fichier ${filename} a été encodé"
    fi

    ## Moving file after encoding
    mv -v "${file}" "${encodedSource}/${filename}.${fileExtension}"

  else
    filename=$(getFilename "${file}" "0")
    echo "Le fichier ${filename} n'est pas à encoder"
  fi

  espace
done

echo "Encodage terminé"
separateur
separateur
separateur
separateur
separateur